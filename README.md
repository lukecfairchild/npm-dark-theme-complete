# NPM Dark Theme Complete

![](images/screenshot.png)

This is a modified version of NPM Dark Theme, adapted to cover the entire site as well as improve a few other aspects of the theme.

You can download the styling [here](https://userstyles.org/styles/169643/npm-dark-theme-complete)
